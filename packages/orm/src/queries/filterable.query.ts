import {DeepPartial} from '../deep-partial';
import {FilterableQueryState} from '../filterable-query-state';
import {FilterOperator} from '../filter-operator';
import {EntitySelector, Selector, TypedSelector} from './functions';
import {State} from './state';
import {TableDescription} from '../dialects/sql.dialect';
import {TablePathColumn} from './selector';
import {FilterLogicalOperator} from '../filter-value';

export interface Cloneable {
  clone(): this;
}

export interface FilterQuery<Entity> {
  where(filter: DeepPartial<Entity>): FilterQuery<Entity>;
  whereLike(selector: TypedSelector<Entity, string>, value: string): FilterQuery<Entity>
  whereOr(builder: (query: FilterQuery<Entity>) => FilterQuery<Entity>): FilterQuery<Entity>
  whereGreater(selector: TypedSelector<Entity, number>, value: number): FilterQuery<Entity>
  whereGreater(selector: TypedSelector<Entity, Date>, value: Date): FilterQuery<Entity>
  whereGreaterEqual(selector: TypedSelector<Entity, number>, value: number): FilterQuery<Entity>
  whereGreaterEqual(selector: TypedSelector<Entity, Date>, value: Date): FilterQuery<Entity>
  whereLower(selector: TypedSelector<Entity, number>, value: number): FilterQuery<Entity>
  whereLower(selector: TypedSelector<Entity, Date>, value: Date): FilterQuery<Entity>
  whereIsNull(selector: Selector<Entity>): FilterQuery<Entity>
  whereEqual(selector: TypedSelector<Entity, boolean>, value: boolean | null): FilterQuery<Entity>
  whereEqual(selector: TypedSelector<Entity, string>, value: string | null): FilterQuery<Entity>
  whereEqual(selector: TypedSelector<Entity, number>, value: number | null): FilterQuery<Entity>
  whereEqual(selector: TypedSelector<Entity, Date>, value: Date | null): FilterQuery<Entity>
  whereLowerEqual(selector: TypedSelector<Entity, number>, value: number): FilterQuery<Entity>
  whereLowerEqual(selector: TypedSelector<Entity, Date>, value: Date): FilterQuery<Entity>;
}

export class FilterableQuery<Entity> implements FilterQuery<Entity> {
  constructor(protected table: TableDescription,
              private stateResolver: () => State<FilterableQueryState>,
              private selectorResolver: (state: State<FilterableQueryState>, selector: TypedSelector<Entity, any>) => TablePathColumn) {

  }

  protected clone(state: State<FilterableQueryState>): this {
    return <any>new FilterableQuery(this.table, this.stateResolver, this.selectorResolver);//TODO
  }

  where(filter: DeepPartial<Entity>): this {
    const newState = this.stateResolver().clone();
    this.recursiveWhere(newState, filter, []);
    return this.clone(newState);
  }

  private recursiveWhere(newState: State<FilterableQueryState>, filter: any, paths: string[]) {
    for (let key in filter) {
      const value = filter[key];
      const path = [...paths, key];

      if (typeof value === 'number') {
        this.buildSelectorForPath(newState, path, value);
      } else if (typeof value === 'string') {
        this.buildSelectorForPath(newState, path, value);
      } else if (value instanceof Date) {
        this.buildSelectorForPath(newState, path, value);
      } else {
        this.recursiveWhere(newState, value, path);
      }
    }
  }

  private buildSelectorForPath(newState: State<FilterableQueryState>, paths: string[], value: any) {
    const entitySelector: EntitySelector<any, any> = entity => {
      let current = entity;
      for (const path of paths) {
        current = current[path];
      }
      return current;
    };
    const path = this.selectorResolver(newState, entitySelector);

    newState.value.where.conditions.push({
      operator: FilterOperator.Equals,
      tableAlias: path.tableAlias,
      column: path.column,
      value: value,
    });
  }

  whereLike(selector: TypedSelector<Entity, string>, value: string): this {
    const newState = this.stateResolver().clone();
    const path = this.selectorResolver(newState, selector);
    newState.value.where.conditions.push({
      operator: FilterOperator.Like,
      tableAlias: path.tableAlias,
      column: path.column,
      value: value,
    });
    return this.clone(newState);
  }

  whereOr(builder: (query: FilterableQuery<Entity>) => FilterableQuery<Entity>): this {
    const state = this.stateResolver().clone();
    state.value.where.operator = FilterLogicalOperator.Or;
    const subQuery = this.clone(state);
    const result = builder(subQuery);
    return this.clone(result.stateResolver());
  }

  whereGreater(selector: TypedSelector<Entity, number>, value: number): this
  whereGreater(selector: TypedSelector<Entity, Date>, value: Date): this
  whereGreater(selector: TypedSelector<Entity, number | Date>, value: number | Date): this {
    const newState = this.stateResolver().clone();
    const path = this.selectorResolver(newState, selector);
    newState.value.where.conditions.push({
      operator: FilterOperator.Greater,
      tableAlias: path.tableAlias,
      column: path.column,
      value: value,
    });
    return this.clone(newState);
  }

  whereGreaterEqual(selector: TypedSelector<Entity, number>, value: number): this
  whereGreaterEqual(selector: TypedSelector<Entity, Date>, value: Date): this
  whereGreaterEqual(selector: TypedSelector<Entity, number | Date>, value: number | Date): this {
    const newState = this.stateResolver().clone();
    const path = this.selectorResolver(newState, selector);
    newState.value.where.conditions.push({
      operator: FilterOperator.GreaterEqual,
      tableAlias: path.tableAlias,
      column: path.column,
      value: value,
    });
    return this.clone(newState);
  }

  whereLower(selector: TypedSelector<Entity, number>, value: number): this
  whereLower(selector: TypedSelector<Entity, Date>, value: Date): this
  whereLower(selector: TypedSelector<Entity, number | Date>, value: number | Date): this {
    const newState = this.stateResolver().clone();
    const path = this.selectorResolver(newState, selector);
    newState.value.where.conditions.push({
      operator: FilterOperator.Lower,
      tableAlias: path.tableAlias,
      column: path.column,
      value: value,
    });
    return this.clone(newState);
  }

  whereIsNull(selector: Selector<Entity>): this {
    const newState = this.stateResolver().clone();
    const path = this.selectorResolver(newState, selector);
    newState.value.where.conditions.push({
      operator: FilterOperator.Equals,
      tableAlias: path.tableAlias,
      column: path.column,
      value: null,
    });
    return this.clone(newState);
  }

  whereEqual(selector: TypedSelector<Entity, boolean>, value: boolean | null): this
  whereEqual(selector: TypedSelector<Entity, string>, value: string | null): this
  whereEqual(selector: TypedSelector<Entity, number>, value: number | null): this
  whereEqual(selector: TypedSelector<Entity, Date>, value: Date | null): this
  whereEqual(selector: TypedSelector<Entity, boolean | string | number | Date | null>, value: boolean | string | number | Date | null): this {
    const newState = this.stateResolver().clone();
    const path = this.selectorResolver(newState, selector);
    newState.value.where.conditions.push({
      operator: FilterOperator.Equals,
      tableAlias: path.tableAlias,
      column: path.column,
      value: value,
    });
    return this.clone(newState);
  }

  whereLowerEqual(selector: TypedSelector<Entity, number>, value: number): this
  whereLowerEqual(selector: TypedSelector<Entity, Date>, value: Date): this
  whereLowerEqual(selector: TypedSelector<Entity, number | Date>, value: number | Date): this {
    const newState = this.stateResolver().clone();
    const path = this.selectorResolver(newState, selector);
    newState.value.where.conditions.push({
      operator: FilterOperator.LowerEqual,
      tableAlias: path.tableAlias,
      column: path.column,
      value: value,
    });
    return this.clone(newState);
  }
}
