import {Sql} from './sql.query';
import {SqlDialect, TableDescription} from '../dialects/sql.dialect';
import {FilterableQuery} from './filterable.query';
import {FilterableQueryState} from '../filterable-query-state';
import {State} from './state';
import {EntitySelector} from './functions';
import {TablePathColumn} from './selector';


export interface DeleteQueryState extends FilterableQueryState {

}

export class DeleteQuery<T> extends FilterableQuery<T> {
  constructor(private dialect: SqlDialect,
              table: TableDescription,
              state: State<DeleteQueryState>) {
    super(table, null, null);
  }

  execute(): Promise<void> {
    return undefined;
  }

  toSQL(): Sql {
    return null;
  }

  clone(state: Partial<any>): this {
    return undefined;
  }

  protected getSelectorKey(newState: State<DeleteQueryState>, selector: EntitySelector<T, any>): TablePathColumn {
    return undefined;
  }
}
