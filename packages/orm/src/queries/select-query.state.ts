import {FilterableQueryState} from '../filterable-query-state';

export enum JoinType {
  Left
}

export interface SelectJoin {
  type: JoinType,
  table: string;
  schema?: string;
  tableAlias: string;
  conditions: { leftTableAlias: string, leftColumn: string, rightTableAlias: string, rightColumn: string }[];
}

export interface SelectColumn {
  name: string;
  alias: string;
  tableAlias: string;
}

export interface SelectTable {
  name: string;
  alias?: string;
  schema?: string;
}

export interface SelectOrderBy {
  tableAlias: string;
  column: string;
  direction: 'ASC' | 'DESC';
}

export interface SelectQueryState extends FilterableQueryState {
  tables: SelectTable[];
  joins: SelectJoin[];
  columns: SelectColumn[];
  skip: number | null;
  limit: number | null;
  orderBy: SelectOrderBy[];
}
