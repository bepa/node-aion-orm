export function now(): DatabaseFunction<any, Date> {
  return null;
}

export function upper<Entity>(sel: Selector<Entity>): DatabaseFunction<Entity, string> {
  return null;
}

export function lower<Entity>(sel: Selector<Entity>): DatabaseFunction<Entity, string> {
  return null;
}

export function avg<Entity>(sel: Selector<Entity>): DatabaseFunction<Entity, number> {
  return null;
}

export function max<Entity>(sel: Selector<Entity>): DatabaseFunction<Entity, number> {
  return null;
}

export function min<Entity>(sel: Selector<Entity>): DatabaseFunction<Entity, number> {
  return null;
}

export function sum<Entity>(sel: Selector<Entity>): DatabaseFunction<Entity, number> {
  return null;
}

export function concat<Entity>(...selectors: Selector<Entity>[]): DatabaseFunction<Entity, string> {
  return null;
}

export function count<Entity>(sel?: EntitySelector<Entity, any>): DatabaseFunction<Entity, number> {
  return null;
}

export interface EntitySelector<Entity, Value> {
  (entity: Entity): Value;
}

export type Selector<Entity> = EntitySelector<Entity, any> | number | Date | string;
export type TypedSelector<Entity, Type> = EntitySelector<Entity, Type> | DatabaseFunction<Entity, Type> | Type;

export interface DatabaseFunction<Entity, Value> {

}
