import {TableDescription} from '../dialects/sql.dialect';
import {DecoratorUtil} from '../decorators/decorator.util';
import {EntitySelector} from './functions';
import {JoinType, SelectJoin} from './select-query.state';

function buildSelector(table: TableDescription) {
  return buildSelectorObject(table, [table.name], []);
}

function buildSelectorObject(table: TableDescription, paths: string[], joins: SelectJoin[]): TablePathColumn {
  const handler = {
    get: function (obj: any, prop: any) {
      if (prop in table.columns) {
        const column = table.columns[prop];
        const path: TablePathColumn = {
          tableAlias: paths.join('.'),
          table: table.name,
          joins: joins,
          column: column.name,
          columnAlias: prop,
        };

        return path;
      }


      const foreignKey = table.foreignKeys[prop];
      if (!foreignKey) {
        if (prop === 'joins') {
          return joins;
        }

        throw new Error(prop + ' not found');
      }

      const refTable = DecoratorUtil.getMetadata(foreignKey.references.prototype);

      return buildSelectorObject(refTable, [...paths, prop], [...joins, {
        table: refTable.name,
        schema: refTable.schema,
        tableAlias: [...paths, prop].join('.'),
        type: JoinType.Left,
        conditions: [
          {
            leftTableAlias: paths.join('.'),
            leftColumn: foreignKey.columnName,
            rightTableAlias: [...paths, prop].join('.'),
            rightColumn: table.primaryKeys[0],
          },
        ],
      }]);
    },
  };

  return new Proxy({}, handler);
}

export interface TablePathColumn {
  table: string;
  tableAlias: string;
  column: string;
  columnAlias: string;
  joins: SelectJoin[];
}


export function getSelectorPath(table: TableDescription, selector: EntitySelector<any, any>): TablePathColumn {
  const selectorArgument = buildSelector(table);
  return selector(selectorArgument) as TablePathColumn;
}
