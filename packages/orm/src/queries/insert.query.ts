import {SqlDialect, TableDescription} from '../dialects/sql.dialect';
import {Sql} from './sql.query';

export class InsertQuery<T> {
  constructor(private dialect: SqlDialect,
              private table: TableDescription,
  ) {

  }

  value(instance: Partial<T>) {

  }

  values(...instances: Partial<T>[]) {

  }

  returning() {

  }

  toSQL(): Sql {
    return null;
  }

  execute(): Promise<void> {
    return undefined;
  }
}
