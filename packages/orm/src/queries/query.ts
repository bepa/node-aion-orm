import {SqlDialect} from '../dialects/sql.dialect';
import {Constructable} from '../index';
import {DecoratorUtil} from '../decorators/decorator.util';
import {InsertQuery} from './insert.query';
import {SelectQuery} from './select.query';
import {UpdateQuery} from './update.query';
import {DeleteQuery, DeleteQueryState} from './delete.query';
import {State} from './state';
import {FilterLogicalOperator} from '../filter-value';

export class Query {
  constructor(private dialect: SqlDialect) {
  }

  insert<T>(table: Constructable<T>): InsertQuery<T> {
    return new InsertQuery(this.dialect, DecoratorUtil.getMetadata(table.prototype));
  }

  select<T>(table: Constructable<T>): SelectQuery<T> {
    return new SelectQuery(this.dialect, DecoratorUtil.getMetadata(table.prototype));
  }

  update<T>(table: Constructable<T>): UpdateQuery<T> {
    return new UpdateQuery(this.dialect, DecoratorUtil.getMetadata(table.prototype));
  }

  delete<T>(table: Constructable<T>): DeleteQuery<T> {
    return new DeleteQuery(this.dialect, DecoratorUtil.getMetadata(table.prototype), new State<DeleteQueryState>({
      where: {operator: FilterLogicalOperator.And, conditions: []},
    }));
  }

}
