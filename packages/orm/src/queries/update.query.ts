import {SqlDialect, TableDescription} from '../dialects/sql.dialect';
import {Sql} from './sql.query';
import {FilterableQuery} from './filterable.query';
import {State} from './state';
import {EntitySelector} from './functions';
import {TablePathColumn} from './selector';

export class UpdateQuery<T> extends FilterableQuery<T> {
  constructor(private dialect: SqlDialect,
              table: TableDescription) {
    super(table, null, null);
  }

  set(value: Partial<T>) {
    return this;
  }

  execute(): Promise<void> {
    return undefined;
  }

  toSQL(): Sql {
    return null;
  }

  clone(state: Partial<any>): this {
    return undefined;
  }

  protected getSelectorKey(newState: State<any>, selector: EntitySelector<T, any>): TablePathColumn {
    return undefined;
  }
}
