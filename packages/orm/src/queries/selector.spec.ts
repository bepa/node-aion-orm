import {Column} from '../decorators/column-decorator';
import {Reference} from '../decorators/reference-decorator';
import {getSelectorPath} from './selector';
import {DecoratorUtil} from '../decorators/decorator.util';
import {EntitySelector} from './functions';
import {expect} from 'chai';
import {Table} from '../decorators/table.decorator';

describe('selector', () => {

  @Table({name: 'user', schema: 'public'})
  class User {
    @Column()
    id: string;

    @Column({name: 'first_name'})
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    username: string;

    @Reference({references: User, column: t => t.id})
    parent: User;

    parentId: string;
  }


  it('should select column', () => {
    const table = DecoratorUtil.getMetadata(User.prototype);
    const selector: EntitySelector<User, any> = entity => entity.id;
    const path = getSelectorPath(table, selector);
    expect(path.column).to.be.equal('id');
    expect(path.table).to.be.equal('user');
    expect(path.tableAlias).to.be.equal('user');
  });
});
