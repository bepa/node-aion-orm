import {SqlDialect, TableDescription} from '../dialects/sql.dialect';
import {Sql} from './sql.query';
import {FilterableQuery} from './filterable.query';
import {EntitySelector} from './functions';
import {getSelectorPath} from './selector';
import {DeepPartial} from '../deep-partial';
import {State} from './state';
import {SelectQueryState} from './select-query.state';
import {FilterLogicalOperator} from '../filter-value';

export class SelectQuery<Entity> extends FilterableQuery<Entity> {
  private readonly state: State<SelectQueryState>;

  constructor(private readonly dialect: SqlDialect,
              table: TableDescription,
              state?: State<SelectQueryState>) {
    super(table,
      () => this.state,
      (newState, selector) => this.getSelectorKey(<any>newState, selector));

    this.state = state || new State<SelectQueryState>({
      tables: [{name: this.table.name, alias: this.table.name, schema: this.table.schema}],
      columns: [],
      skip: null,
      limit: null,
      joins: [],
      orderBy: [],
      where: {operator: FilterLogicalOperator.And, conditions: []},
    })
  }

  protected clone(state: State<SelectQueryState>): this {
    return <any>new SelectQuery<Entity>(this.dialect, this.table, state);
  }

  column(...selectors: EntitySelector<Entity, number | string | Date>[]): SelectQuery<Entity> {
    const newState = this.state.clone();
    for (const selector of selectors) {
      const path = this.getSelectorKey(newState, selector);
      newState.value.columns.push({name: path.column, alias: path.columnAlias, tableAlias: path.tableAlias});
    }
    return this.clone(newState);
  }

  include(...selectors: EntitySelector<Entity, object>[]): SelectQuery<Entity> {
    const newState = this.state.clone();
    for (const selector of selectors) {
      this.getSelectorKey(newState, selector);
    }
    return this.clone(newState);
  }

  orderBy(...selectors: EntitySelector<Entity, number | string | Date>[]): SelectQuery<Entity> {
    const newState = this.state.clone();
    for (const selector of selectors) {
      const path = this.getSelectorKey(newState, selector);
      newState.value.orderBy.push({column: path.column, tableAlias: path.tableAlias, direction: 'ASC'});
    }
    return this.clone(newState);
  }

  orderByDesc(...selectors: EntitySelector<Entity, number | string | Date>[]): SelectQuery<Entity> {
    const newState = this.state.clone();
    for (const selector of selectors) {
      const path = this.getSelectorKey(newState, selector);
      newState.value.orderBy.push({column: path.column, tableAlias: path.tableAlias, direction: 'DESC'});
    }
    return this.clone(newState);
  }

  skip(value: number | null): SelectQuery<Entity> {
    const newState = this.state.clone();
    newState.value.skip = value;
    return this.clone(newState);
  }

  limit(value: number | null): SelectQuery<Entity> {
    const newState = this.state.clone();
    newState.value.limit = value;
    return this.clone(newState);
  }

  protected getSelectorKey(newState: State<SelectQueryState>, selector: EntitySelector<Entity, any>) {
    const path = getSelectorPath(this.table, selector);
    for (const join of path.joins) {
      if (!newState.value.joins.some(existing => existing.tableAlias === join.tableAlias)) {
        newState.value.joins.push(join);
      }
    }
    return path;
  }

  toSQL(): Sql {
    return this.getSql(this.state.clone());
  }

  private getSql(state: State<SelectQueryState>) {
    if (state.value.columns.length === 0) {
      for (const key in this.table.columns) {
        const column = this.table.columns[key];
        state.value.columns.push({
          name: column.name,
          tableAlias: this.table.name,
          alias: key,
        });
      }
    }
    return this.dialect.select(state.value);
  }

  async execute(): Promise<DeepPartial<Entity>[]> {
    const sql = this.toSQL();
    const result = await this.dialect.execute(sql);
    return result.rows as any[];
  }

  async executeFirst(): Promise<DeepPartial<Entity> | null> {
    const state = this.state.clone();
    state.value.limit = 1;
    const sql = this.getSql(state);
    const result = await this.dialect.execute(sql);
    return result.rows[0] || null;
  }
}
