import {cloneDeep} from 'lodash';

export class State<Value> {
  constructor(public value: Value) {

  }

  clone(): State<Value> {
    return new State<Value>(cloneDeep(this.value));
  }
}
