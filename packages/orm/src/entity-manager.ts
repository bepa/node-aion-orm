import {Constructable} from './index';
import {SqlDialect, TableDescription} from './dialects/sql.dialect';
import {InsertQuery} from './queries/insert.query';
import {SelectQuery} from './queries/select.query';
import {UpdateQuery} from './queries/update.query';
import {DeleteQuery, DeleteQueryState} from './queries/delete.query';
import {DecoratorUtil} from './decorators/decorator.util';
import {State} from './queries/state';
import {FilterLogicalOperator} from './filter-value';

export class EntityRepository<T> {
  private readonly table: TableDescription;

  constructor(private dialect: SqlDialect,
              private entity: Constructable<any>) {
    this.table = DecoratorUtil.getMetadata(entity.prototype);
  }

  insert(): InsertQuery<T> {
    return new InsertQuery(this.dialect, this.table);
  }

  select(): SelectQuery<T> {
    return new SelectQuery(this.dialect, this.table);
  }

  update(): UpdateQuery<T> {
    return new UpdateQuery(this.dialect, this.table);
  }

  delete(): DeleteQuery<T> {
    return new DeleteQuery(this.dialect, this.table, new State<DeleteQueryState>({
      where: {operator: FilterLogicalOperator.And, conditions: []},
    }));
  }

}

export interface EntityManagerConfiguration {
  dialect: SqlDialect;
  entities?: Constructable<any>[];
  sequences?: Constructable<any>[];
}

export class EntityManager {
  constructor(private config: EntityManagerConfiguration) {

  }

  entity<T>(entity: Constructable<T>): EntityRepository<T> {
    return new EntityRepository<T>(this.config.dialect, entity);
  }

  sequence(type: Constructable<any>): SequenceRepository {
    return new SequenceRepository(this.config.dialect, type);
  }

  async transaction(trx: (entityManager: EntityManager) => Promise<void> | void): Promise<void> {

  }

  raw(sql: string) {

  }

  migrateSchema() {
    for (const entity of this.config.entities) {
      //console.log(this.config.dialect.createTable(DecoratorUtil.getMetadata(entity.prototype)));
    }
  }
}

export class SequenceRepository {
  constructor(private dialect: SqlDialect,
              type: Constructable<any>) {

  }

  next(): Promise<number> {
    return null;
  }
}
