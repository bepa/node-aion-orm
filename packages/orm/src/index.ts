export interface Constructable<T> extends Function {
    new(...args: any[]): T;
}

export enum DataType {
    String,
    Json,
    Boolean,
    Integer,
    Timestamp,
}

export * from './decorators';
export * from './dialects';
export * from './queries';
export * from './deep-partial';
export * from './entity-manager';
export * from './filter-operator';
export * from './filter-value';
export * from './filterable-query-state';
