export enum FilterOperator {
  Equals,
  Greater,
  GreaterEqual,
  Lower,
  LowerEqual,
  Like,
}
