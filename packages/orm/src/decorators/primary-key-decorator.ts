import {DecoratorUtil} from './decorator.util';

export const PrimaryKey: PropertyDecorator = (target, propertyKey) => {
  DecoratorUtil.mergeMetadata(target, {primaryKeys: [propertyKey.toString()]});
};
