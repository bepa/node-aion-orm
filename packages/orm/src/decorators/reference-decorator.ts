import {Constructable} from '../index';
import {TableDescription, TableForeignKey, TableForeignKeyActions} from '../dialects/sql.dialect';
import {DeepPartial} from '../deep-partial';
import {DecoratorUtil} from './decorator.util';
import {getSelectorPath} from '../queries/selector';

export function Reference<R>(options: {
  references: Constructable<R>,
  column?: (ref: R) => any;
  onDelete?: TableForeignKeyActions,
  onUpdate?: TableForeignKeyActions,
  name?: string
}): PropertyDecorator {
  return (target, propertyKey) => {
    const metadata: DeepPartial<TableDescription> = {foreignKeys: {}};
    metadata.foreignKeys[propertyKey.toString()] = <TableForeignKey>{
      name: (options ? options.name : propertyKey.toString()) || propertyKey.toString(),
      references: options.references,
      columnName: options.column ? getSelectorPath(DecoratorUtil.getMetadata(options.references.prototype), options.column).column : propertyKey.toString() + 'Id',
      onDelete: options ? options.onDelete : TableForeignKeyActions.NoAction,
      onUpdate: options ? options.onUpdate : TableForeignKeyActions.NoAction,
    };
    DecoratorUtil.mergeMetadata(target, metadata);
  };
}
