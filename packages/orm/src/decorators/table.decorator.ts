import {DecoratorUtil} from './decorator.util';

export function Table(config: { name?: string, schema?: string }): ClassDecorator {
  return target => {
    DecoratorUtil.mergeMetadata(target.prototype, {name: config.name, schema: config.schema});
  };
}
