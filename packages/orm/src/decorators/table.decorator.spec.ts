import {Table} from './table.decorator';
import {DecoratorUtil} from './decorator.util';
import {expect} from 'chai';

@Table({name: 'foo', schema: 'bar'})
class TestTable {

}

describe('table.decorator', () => {
  it('should set table name and schema', () => {
    const table = DecoratorUtil.getMetadata(TestTable.prototype);
    expect(table.name).to.be.equal('foo');
    expect(table.schema).to.be.equal('bar');
  });
});
