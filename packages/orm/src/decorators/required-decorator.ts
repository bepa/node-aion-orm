import {DecoratorUtil} from './decorator.util';
import {TableDescription} from '../dialects/sql.dialect';
import {DeepPartial} from '../deep-partial';

export const Required: PropertyDecorator = (target, propertyKey) => {
  const metadata: DeepPartial<TableDescription> = {columns: {}};
  metadata.columns[propertyKey.toString()] = { required: true };
  DecoratorUtil.mergeMetadata(target, metadata);
};
