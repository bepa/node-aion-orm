import {DataType} from '../index';
import {DecoratorUtil} from './decorator.util';
import {TableDescription} from '../dialects/sql.dialect';
import {DeepPartial} from '../deep-partial';

export function Column(config?: { type?: DataType, name?: string }): PropertyDecorator {
  return (target, propertyKey) => {
    const designType = Reflect.getMetadata('design:type', target, propertyKey);
    const metadata: DeepPartial<TableDescription> = {
      columns: {},
    };

    let type = null;
    if (designType === String) {
      type = DataType.String;
    } else if (designType === Date) {
      type = DataType.Timestamp;
    } else if (designType === Number) {
      type = DataType.Integer;
    } else if (designType === Boolean) {
      type = DataType.Boolean;
    }

    metadata.columns[propertyKey.toString()] = {
      type: type,
      name: (config ? config.name : propertyKey.toString()) || propertyKey.toString(),
    };
    DecoratorUtil.mergeMetadata(target, metadata);
  };
}

