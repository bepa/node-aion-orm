import 'reflect-metadata';
import * as deepmerge from 'deepmerge';
import {TableDescription} from '../dialects/sql.dialect';
import {DeepPartial} from '../deep-partial';

export class DecoratorUtil {
  static mergeMetadata(target: any, metadata: DeepPartial<TableDescription>) {
    const existingMetadata = this.getMetadata(target);
    const mergedMetadata = deepmerge(existingMetadata, metadata);
    Reflect.defineMetadata('entityMetadata', mergedMetadata, target);
  }

  static getMetadata(target: any): TableDescription {
    const metadata = Reflect.getMetadata('entityMetadata', target);
    if (metadata) {
      return metadata;
    }

    return <TableDescription>{
      columns: {},
      indices: {},
      primaryKeys: [],
      foreignKeys: {},
      name: target.constructor.name,
    };
  }
}
