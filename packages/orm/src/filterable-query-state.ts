import {FilterValue} from './filter-value';

export interface FilterableQueryState {
  where: FilterValue;
}


