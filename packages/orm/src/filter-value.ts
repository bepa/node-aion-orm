import {FilterOperator} from './filter-operator';

export enum FilterLogicalOperator {
  And,
  Or,
}

export interface FilterValue {
  operator: FilterLogicalOperator;
  conditions: (Condition | FilterValue)[];
}

export interface Condition {
  tableAlias: string;
  column: string;
  value: any;
  operator: FilterOperator;
}
