import {Constructable, DataType} from '../index';
import {Sql} from '../queries/sql.query';
import {SelectQueryState} from '../queries/select-query.state';

export interface SqlDialect {
  execute(sql: Sql): Promise<{ rowCount: number, rows: any[] }>;
  select(query: SelectQueryState): Sql;
}

export class TableDescription {
  name: string;
  schema: string;
  columns: { [key: string]: TableColumn };
  primaryKeys: string[];
  foreignKeys: { [key: string]: TableForeignKey };
  indices: { [key: string]: TableIndex };
}

export enum TableForeignKeyActions {Cascade, NoAction, Restrict}

export class TableForeignKey {
  references: Constructable<any>;
  name: string;
  columnName: string;
  onDelete: TableForeignKeyActions;
  onUpdate: TableForeignKeyActions;
}

export class TableColumn {
  name: string;
  type: DataType;
  required: boolean;
  default: any | null;
}

export class TableIndex {
  name: string;
  unique: boolean;
}

