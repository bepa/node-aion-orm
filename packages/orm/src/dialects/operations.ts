export interface AlterTableOperation {
  schema: string;
  oldTableName: string;
  newTableName: string;
}

export interface AlterColumnOperation {
  schema: string;
  table: string;
  oldColumnName: string;
  newColumnName: string;
}

export interface DropTableOperation {
  schema: string;
  tableName: string;
}

export interface DropColumnOperation {
  schema: string;
  tableName: string;
  columnName: string;
}

export interface CreateTableOperation {
  schema: string;
  tableName: string;
  columns: {}[];
}

export interface CreateIndexOperation {
  
}

export interface DropIndexOperation {
  
}

export interface RenameColumnOperation {
  
}

export interface RenameTableOperation {

}

export interface RenameIndexOperation {

}


export interface AlterIndexOperation {
}

{

}
