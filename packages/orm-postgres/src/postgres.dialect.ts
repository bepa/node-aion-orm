import {Client, ClientConfig} from 'pg';
import {
  Table,
  Column,
  SqlDialect,
  Query,
  FilterOperator,
  JoinType,
  FilterValue,
  FilterLogicalOperator, Condition, SelectQueryState, DataType, TableForeignKeyActions, TableDescription, Sql,
} from 'node-aion-orm';

@Table({ schema: 'information_schema', name: 'tables' })
export class InfoSchemaTables {
  @Column()
  table_catalog: string;

  @Column()
  table_schema: string;

  @Column()
  table_name: string;


  @Column()
  table_type: string;
}

@Table({ schema: 'information_schema', name: 'columns'})
export class InfoSchemaColumns {
  @Column()
  table_catalog: string;

  @Column()
  table_schema: string;

  @Column()
  table_name: string;

  @Column()
  column_name: string;

  @Column()
  data_type: string;

  @Column()
  ordinal_position: number;
}

@Table({schema: 'information_schema', name: 'table_constraints'})
export class InfoSchemaTableConstraints {
  @Column()
  table_schema: string;

  @Column()
  table_name: string;

  @Column()
  constraint_name: string;

  @Column()
  constraint_type: string;
}

@Table({schema: 'information_schema', name: 'key_column_usage'})
export class InfoSchemaKeyColumnUsage {
  @Column()
  table_schema: string;

  @Column()
  constraint_name: string;

  @Column()
  column_name: string;
}

@Table({schema: 'information_schema', name: 'constraint_column_usage'})
export class InfoSchemaConstraintColumnUsage {
  @Column()
  table_schema: string;

  @Column()
  table_name: string;

  @Column()
  constraint_name: string;

  @Column()
  column_name: string;
}

export class PostgresDialect implements SqlDialect {
  private client: Client;
  private connected: Promise<void>;
  private query: Query;

  constructor(private config: ClientConfig) {
    this.client = new Client(config);
    this.connected = this.client.connect();
    this.query = new Query(this);
  }

  async close() {
    await this.client.end();
  }

  createTable(table: TableDescription) {
    const attributes = [];

    const columns = Object.keys(table.columns).map(key => {
      const column = table.columns[key];
      return `"${column.name}" ${this.getDataType(column.type)}${column.required ? ' NOT NULL' : ''}`
    });

    attributes.push(...columns);

    const primaryKeys = table.primaryKeys.map(key => `"${table.columns[key].name}"`);
    if (primaryKeys.length > 0) {
      attributes.push(`PRIMARY KEY (${primaryKeys.join(',')})`);
    }

    const foreignKeys = Object.keys(table.foreignKeys)
      .map(key => {
        const foreignKey = table.foreignKeys[key];
        return `FOREIGN KEY ("${table.columns[key].name}") REFERENCES "${foreignKey.references.name}" ON DELETE ${this.getForeignKeyAction(foreignKey.onDelete)} ON UPDATE ${this.getForeignKeyAction(foreignKey.onUpdate)}`;
      });
    if (foreignKeys.length > 0) {
      attributes.push(...foreignKeys);
    }

    return `CREATE TABLE "${table.name}" (${attributes.join(', ')})`;
  }

  async getTables(schema: string): Promise<TableDescription[]> {
    const tables = await this.query.select(InfoSchemaTables)
      .where({
        table_schema: schema,
      }).execute();

    const columns = await this.query.select(InfoSchemaColumns)
      .where({
        table_schema: schema,
      }).execute();

    const tableConstraints = await this.query.select(InfoSchemaTableConstraints)
      .where({
        constraint_type: 'FOREIGN KEY',
      }).execute();

    const result: TableDescription[] = [];
    for(const table of tables) {
      const description: TableDescription = {
        schema: table.table_schema,
        name: table.table_name,
        columns: {},
        foreignKeys: {},
        indices: {},
        primaryKeys: [],
      };
      for(const column of columns) {
        if(table.table_name !== column.table_name || table.table_schema !== column.table_schema) {
          continue;
        }
        description.columns[column.column_name] = {
          name: column.column_name,
          required: false,
          type: null,
          default: null,
        };
      }
      result.push(description);
    }

    return result;
  }

  alterTable(existingTable: TableDescription, newTable: TableDescription) {

  }

  private getForeignKeyAction(action: TableForeignKeyActions) {
    switch (action) {
      case TableForeignKeyActions.Cascade:
        return 'CASCADE';
      case TableForeignKeyActions.NoAction:
        return 'NO ACTION';
      case TableForeignKeyActions.Restrict:
        return 'RESTRICT';
    }
  }

  private getDataType(type: DataType) {
    switch (type) {
      case DataType.Boolean:
        return 'boolean';
      case DataType.Integer:
        return 'int';
      case DataType.Json:
        return 'jsonb';
      case DataType.String:
        return 'varchar';
      case DataType.Timestamp:
        return 'timestamptz';
    }
  }

  async execute(sql: Sql): Promise<any> {
    await this.connected;

    const res = await this.client.query(sql.query, sql.params);
    return {
      rows: res.rows,
      rowCount: res.rowCount,
    };
  }

  select(query: SelectQueryState): Sql {
    const values: any[] = [];

    const columns = query.columns
      .map(column => {
        if (column.name === column.alias) {
          return `"${column.tableAlias}"."${column.name}"`;
        } else {
          return `"${column.tableAlias}"."${column.name}" AS "${column.alias}"`;
        }
      })
      .join(', ');

    const tables = query.tables
      .map(table => {
        const schema = table.schema ? `"${table.schema}".` : '';

        if (table.alias === table.name) {
          return `${schema}"${table.name}"`;
        } else {
          return `${schema}"${table.name}" "${table.alias}"`;
        }
      })
      .join(', ');

    const conditions = this.buildCondition(query.where, values);

    const joins = query.joins.map(join => {
      const conditions = join.conditions.map(condition => `"${condition.leftTableAlias}"."${condition.leftColumn}" = "${condition.rightTableAlias}"."${condition.rightColumn}"`).join(' AND ');
      const schema = join.schema ? `"${join.schema}".` : '';
      return `${join.type === JoinType.Left ? 'LEFT' : 'INNER'} JOIN ${schema}"${join.table}" "${join.tableAlias}" ON ${conditions}`
    });

    const orderBy = query.orderBy.map(order => {
      return `"${order.tableAlias}"."${order.column}" ${order.direction}`
    }).join(', ');

    let sql = `SELECT ${columns} FROM ${tables}`;

    if (joins.length > 0) {
      sql += ' ' + joins.join(' ');
    }

    if (conditions.length > 0) {
      sql += ` WHERE ${conditions}`;
    }

    if (orderBy.length > 0) {
      sql += ` ORDER BY ${orderBy}`;
    }

    if (query.limit !== null) {
      sql += ` LIMIT ${query.limit}`;
    }

    if (query.skip !== null) {
      sql += ` OFFSET ${query.skip}`;
    }

    return {query: sql, params: values};
  }

  private getCompareOperator(operator: FilterOperator) {
    switch (operator) {
      case FilterOperator.Equals:
        return '=';
      case FilterOperator.Greater:
        return '>';
      case FilterOperator.GreaterEqual:
        return '>=';
      case FilterOperator.Like:
        return 'LIKE';
      case FilterOperator.Lower:
        return '<';
      case FilterOperator.LowerEqual:
        return '<=';
    }
  }

  private buildCondition(filter: FilterValue, values: any[]): string {
    const op = filter.operator === FilterLogicalOperator.And ? ' AND ' : ' OR ';
    const start = filter.operator === FilterLogicalOperator.And ? '' : '(';
    const end = filter.operator === FilterLogicalOperator.And ? '' : ')';

    return start + filter.conditions.map(condition => {
      if ((<FilterValue>condition).conditions) {
        return this.buildCondition((<FilterValue>condition), values);
      } else {
        const con = (<Condition>condition);
        if (con.operator === FilterOperator.Equals && (con.value === null || con.value === undefined)) {
          const sql = `"${con.tableAlias}"."${con.column}" IS NULL`;
          return sql;
        } else {
          const sql = `"${con.tableAlias}"."${con.column}" ${this.getCompareOperator(con.operator)} $${values.length + 1}`;
          values.push(con.value);
          return sql;
        }

      }
    }).join(op) + end;
  }
}
