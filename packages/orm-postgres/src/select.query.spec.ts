import {expect} from 'chai';
import {Column, EntityManager, PrimaryKey, Reference} from 'node-aion-orm';
import {PostgresDialect} from './postgres.dialect';

export class User {
  @Column()
  @PrimaryKey
  id: string;

  @Column({name: 'first_name'})
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  username: string;

  @Column()
  parentId: string;

  @Column()
  followers: number;

  @Reference({
    references: User,
  })
  parent: User;

  @Column()
  created: Date;
}

let dialect: PostgresDialect;
let em: EntityManager;

describe('select.query', () => {
  before(() => {
    dialect = new PostgresDialect({
      user: 'postgres',
      host: process.env.POSTGRES_HOST || 'localhost',
      database: 'orm',
      password: 'postgres',
      port: 5432,
    });
    em = new EntityManager({
      dialect: dialect,
      entities: [User],
      sequences: [],
    });
  });

  after(() => dialect.close());

  it('select().column(id)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User"');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id).include(u => u.parent)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .include(u => u.parent);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User" LEFT JOIN "User" "User.parent" ON "User"."parentId" = "User.parent"."id"');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(parent.username).include(u => u.parent)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.parent.username);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User.parent"."username" FROM "User" LEFT JOIN "User" "User.parent" ON "User"."parentId" = "User.parent"."id"');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id).orderBy(u => u.created)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .orderBy(u => u.created);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User" ORDER BY "User"."created" ASC');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id).orderByDesc(u => u.created)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .orderByDesc(u => u.created);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User" ORDER BY "User"."created" DESC');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id).skip(10)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .skip(10);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User" OFFSET 10');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id).skip(10).skip(null)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .skip(10)
      .skip(null);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User"');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id).limit(10)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .limit(10);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User" LIMIT 10');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id).skip(5).limit(10)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .skip(5)
      .limit(10);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User" LIMIT 10 OFFSET 5');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id, username)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id, u => u.username);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id", "User"."username" FROM "User"');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id).column(username)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .column(u => u.username);

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id", "User"."username" FROM "User"');
    expect(sql.params).deep.equal([]);
  });

  it('select()', () => {
    const query = em.entity(User)
      .select();

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id", "User"."first_name" AS "firstName", ' +
      '"User"."lastName", "User"."username", "User"."parentId", "User"."followers", "User"."created" FROM "User"');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id).where({ username: "123" })', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .where({
        username: '123',
      });

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE "User"."username" = $1');
    expect(sql.params).deep.equal(['123']);
  });

  it('select().column(id).where({ followers: 9 })', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .where({
        followers: 9,
      });

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE "User"."followers" = $1');
    expect(sql.params).deep.equal([9]);
  });

  it('select().column(id).where({ created: new Date() })', () => {
    const dateValue = new Date();
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .where({
        created: dateValue,
      });

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE "User"."created" = $1');
    expect(sql.params).deep.equal([dateValue]);
  });

  it('select().column(id).where({ parent: { username: "abc" } })', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .where({
        parent: {
          username: 'abc',
        },
      });

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User" ' +
      'LEFT JOIN "User" "User.parent" ON "User"."parentId" = "User.parent"."id" ' +
      'WHERE "User.parent"."username" = $1');
    expect(sql.params).deep.equal(['abc']);
  });

  it('select().column(id).whereEqual(u => u.parent.username, "abc")', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .whereEqual(u => u.parent.username, 'abc');

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User" ' +
      'LEFT JOIN "User" "User.parent" ON "User"."parentId" = "User.parent"."id" ' +
      'WHERE "User.parent"."username" = $1');
    expect(sql.params).deep.equal(['abc']);
  });

  it('select().column(id).where({ parent: { parent: { username: "abc" } } })', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .where({
        parent: {
          parent: {
            username: 'abc',
          },
        },
      });

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User" ' +
      'LEFT JOIN "User" "User.parent" ON "User"."parentId" = "User.parent"."id" ' +
      'LEFT JOIN "User" "User.parent.parent" ON "User.parent"."parentId" = "User.parent.parent"."id" ' +
      'WHERE "User.parent.parent"."username" = $1');
    expect(sql.params).deep.equal(['abc']);
  });

  it('select().column(id).whereEqual(u => u.parent.parent.username, "abc")', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .whereEqual(u => u.parent.parent.username, 'abc');

    const sql = query.toSQL();
    expect(sql.query).to.equal('SELECT "User"."id" FROM "User" ' +
      'LEFT JOIN "User" "User.parent" ON "User"."parentId" = "User.parent"."id" ' +
      'LEFT JOIN "User" "User.parent.parent" ON "User.parent"."parentId" = "User.parent.parent"."id" ' +
      'WHERE "User.parent.parent"."username" = $1');
    expect(sql.params).deep.equal(['abc']);
  });

  it('select().column(id).whereLike(u => u.username, "abc%")', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .whereLike(u => u.username, 'abc%');

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE "User"."username" LIKE $1');
    expect(sql.params).deep.equal(['abc%']);
  });

  it('select().column(id).whereGreater(u => u.followers, 12)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .whereGreater(u => u.followers, 12);

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE "User"."followers" > $1');
    expect(sql.params).deep.equal([12]);
  });

  it('select().column(id).whereGreaterEqual(u => u.followers, 12)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .whereGreaterEqual(u => u.followers, 12);

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE "User"."followers" >= $1');
    expect(sql.params).deep.equal([12]);
  });

  it('select().column(id).whereLower(u => u.followers, 12)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .whereLower(u => u.followers, 12);

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE "User"."followers" < $1');
    expect(sql.params).deep.equal([12]);
  });

  it('select().column(id).whereLowerEqual(u => u.followers, 12)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .whereLowerEqual(u => u.followers, 12);

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE "User"."followers" <= $1');
    expect(sql.params).deep.equal([12]);
  });

  it('select().column(id).whereIsNull(u => u.parentId)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .whereIsNull(u => u.parentId);

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE "User"."parentId" IS NULL');
    expect(sql.params).deep.equal([]);
  });

  it('select().column(id).whereEqual(u => u.followers, 10)', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .whereEqual(u => u.followers, 10);

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE "User"."followers" = $1');
    expect(sql.params).deep.equal([10]);
  });

  it('select().column(id).whereOr(b => b.whereEqual(u => u.username, "abc").whereEqual(u => u.username, "xyz"))', () => {
    const query = em.entity(User)
      .select()
      .column(u => u.id)
      .whereOr(b => b
        .whereEqual(u => u.username, 'abc')
        .whereEqual(u => u.username, 'xyz'));

    const sql = query.toSQL();
    expect(sql.query).to.equal(
      'SELECT "User"."id" FROM "User" ' +
      'WHERE ("User"."username" = $1 OR "User"."username" = $2)');
    expect(sql.params).deep.equal(['abc', 'xyz']);
  });
});
