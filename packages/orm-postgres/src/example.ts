import 'reflect-metadata';
import {Column, concat, Default, EntityManager, Index, lower, PrimaryKey, Reference, Required} from 'node-aion-orm';
import {PostgresDialect} from './postgres.dialect';

export class User {
  @PrimaryKey
  @Column()
  id: string;

  @Required
  @Column()
  @Index({unique: true})
  username: string;

  @Required
  @Column()
  password: string;

  @Required
  @Column()
  email: string;

  @Required
  @Column()
  @Default(false)
  disabled: boolean;

  @Column()
  supervisorId: string;

  @Reference({references: User})
  supervisor: User;

  @Column()
  created: Date;
}

export class Role {
  @PrimaryKey
  @Column()
  id: string;

  @Required
  @Column()
  name: string;
}

export class UserGroup {
  @PrimaryKey
  @Column()
  id: string;

  @Required
  @Column()
  name: string;
}

export class UserGroupUser {
  @Required
  @PrimaryKey
  @Column()
  userId: string;

  @Reference({references: User, column: t => t.id})
  user: User;

  @Required
  @PrimaryKey
  @Column()
  userGroupId: string;

  @Reference({references: UserGroup})
  userGroup: UserGroup;
}

export class BillSequence {

}

async function demo() {

  const dialect = new PostgresDialect({
    user: 'postgres',
    host: process.env.POSTGRES_HOST || 'localhost',
    database: 'orm',
    password: 'postgres',
    port: 5432,
  });

  const em = new EntityManager({
    dialect: dialect,
    entities: [User, Role, UserGroup, UserGroupUser],
    sequences: [BillSequence],
  });

  const query = em
    .entity(UserGroupUser)
    .select()
    .column(u => u.userGroup.name, u => u.userId)
    .where({
      userGroup: {
        id: 'as',
      },
      user: {
        id: 'abc',
      },
    })
    .whereLike(u => u.userId, 'test')
    .whereIsNull(u => u.user.disabled)
    .whereEqual(u => u.userId, '1')
    .skip(10)
    .limit(100)
    .orderBy(e => e.user.username)
    .include(e => e.userGroup)
    .include(e => e.user);

  const entity = await query.executeFirst();

  em.entity(User)
    .select()
    .column()
    .whereOr(builder => builder
      .whereEqual(u => u.username, 'foo')
      .whereOr(subBuilder =>
        subBuilder.whereGreater(u => u.created, new Date()),
      ),
    )
    .whereEqual(lower(u => u.email), 'test')
    .whereLike(concat(u => u.email, ' ', u => u.username), 'test');
  //.groupBy(u => u.email, u => u.username);

  em.entity(User)
    .update()
    .set({
      email: 'test',
    })
    .where({
      id: 'a',
    });

  em.entity(User)
    .insert()
    .value({
      email: 'test@example.com',
      //created: now(),
    });

  em.transaction(async (trx) => {
    await trx.entity(User).insert().value({
      username: 'foo',
    });
  });

  em.entity(User)
    .delete()
    .whereLike(u => u.email, '%@example.com');

// query.executeFirst().then(res => {
//
// });

  const sequence = em.sequence(BillSequence);
//sequence.next()

  em.raw('');

  em.migrateSchema();
}


