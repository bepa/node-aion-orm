import {PostgresDialect} from './postgres.dialect';

let dialect: PostgresDialect;

describe('postgres.dialect', () => {
  before(() => {
    dialect = new PostgresDialect({
      user: 'postgres',
      host: process.env.POSTGRES_HOST || 'localhost',
      database: 'orm',
      password: 'postgres',
      port: 5432,
    });
  });

  after(() => dialect.close());

  it('getTables', async () => {
    await dialect.getTables('public');
  });
});
